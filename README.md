<h1>Beers Of The World</h1>
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/icon_app.png?alt=media&token=360fc388-d3aa-4913-990a-9e4495964172" width="25%"></img>

----------------------------------------------------------------

<b>Beers Of The World</b> es una aplicación de cliente para [PUNK API](https://punkapi.com/) en Android, creada con Java.



## Arquitectura
* Arquitectura MVP
* Uso de <b>ProGuard</b> para ofuscar el codigo del apk productivo
* Utiliza componentes de la arquitectura de Android [viewBinding, Room].
* Inicio de sesión, recuperación de contraseña y creación de usuario mediante Firebase!
* Interfaz de usuario limpia, intuitiva, creada usando las directrices de Material Design
* Utiliza [RxJava](https://github.com/ReactiveX/RxJava) para llamadas de red, transformaciones y observación de bases de datos.
* Completamente fuera de línea. <b>Beers Of The World</b> usa [Room Database](https://developer.android.com/topic/libraries/architecture/room) para administrar una base de datos SQLite local, lo que significa que si ya vio una vez alguna cerveza, esta quedara almacenada y podra verla asi como su detalle incluso si no tiene acceso a la red, cabe señalar que en la primera vez que abre el App al navegar entre las cervezas, estas se iran descargando por paginas al llegar al final del scroll de la pantalla.
* Utiliza [Retrofit](https://square.github.io/retrofit/) para realizar llamadas a la API.
* Utiliza [Glide](https://github.com/bumptech/glide) para cargar imágenes.
* Construido sobre una arquitectura de actividad única. Cada pantalla de la aplicación es un fragmento (menos la actividad principal que es el padre de los fragmentos y la actividad de sesión que es el padre del modulo de sesión).

## Características
* Inicio de sesión
* Recuperación de contraseña
* Creación de usuario
* Visualización de una lista de cervezas
* Búsqueda de cervezas
* Vea los detalles de la cerveza seleccionada, como su imagen, nombre, detalle y tips cerveceros
* Funciona sin conexión mediante el almacenamiento local en base de datos
* Posibilidad de guardar las cervezas como favoritas y verlas en un apartado independiente

## Screenshots
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/screen1.png?alt=media&token=d39ce439-3b84-4401-8011-21622d6713c6" width="30%"></img> 
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/screen2.png?alt=media&token=ad146267-9c0b-4fb6-aed4-b393df1c722e" width="30%"></img> 
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/screen3.png?alt=media&token=6e265380-d89d-4baa-a194-d35e3da9275f" width="30%"></img> 
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/screen4.png?alt=media&token=78037e57-6db0-421d-9228-3ce2826ec4b8" width="30%"></img> 
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/screen5.png?alt=media&token=353beb35-6fad-4282-a8e6-b6493b840e14" width="30%"></img> 
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/screen6.png?alt=media&token=a87ad4dc-ee40-4d67-9197-052b8d475070" width="30%"></img> 
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/screen7.png?alt=media&token=d78c2980-6334-465a-a34d-3ceac40676b6" width="30%"></img> 
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/screen8.png?alt=media&token=5238a5b2-a7fe-4fa4-949c-b9dc72ecf213" width="30%"></img>
<img src="https://firebasestorage.googleapis.com/v0/b/beersoftheworld-23f94.appspot.com/o/recoverpass.png?alt=media&token=b718e217-0e96-4b53-8b76-0d2c8cd30373" width="100%"></img> 
