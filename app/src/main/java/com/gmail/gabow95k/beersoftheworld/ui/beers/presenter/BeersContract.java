package com.gmail.gabow95k.beersoftheworld.ui.beers.presenter;

import com.gmail.gabow95k.beersoftheworld.base.BaseView;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;
import com.gmail.gabow95k.beersoftheworld.model.Beer;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface BeersContract {
    interface View extends BaseView {

        void noBeers();

        void setBeers(List<BeerEntity> beers);
    }

    interface Presenter {

        void getBeersFromApi(int page);

        void checkIfExistDataInDB(int page);
    }

    interface Interactor {

        Single<List<Beer>> getBeers(int page);

        Single<Boolean> checkIfExistDataInDB();

        Single<List<BeerEntity>> getBeersFromDB();

        Completable insertInDB(List<BeerEntity> beers);
    }
}
