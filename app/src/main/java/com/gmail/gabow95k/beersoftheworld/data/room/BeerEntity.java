package com.gmail.gabow95k.beersoftheworld.data.room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.gmail.gabow95k.beersoftheworld.constants.Constants;

import java.util.List;

@Entity(tableName = Constants.BEER_TABLE_NAME)
public class BeerEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "tagLine")
    private String tagLine;
    @ColumnInfo(name = "description")
    private String description;
    @ColumnInfo(name = "foodParing")
    private List<String> foodParing;
    @ColumnInfo(name = "tips")
    private String tips;
    @ColumnInfo(name = "image")
    private String image;
    @ColumnInfo(name = "favorite")
    private Boolean isFav;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getFoodParing() {
        return foodParing;
    }

    public void setFoodParing(List<String> foodParing) {
        this.foodParing = foodParing;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean isFav() {
        return isFav;
    }

    public void setFav(Boolean fav) {
        isFav = fav;
    }
}
