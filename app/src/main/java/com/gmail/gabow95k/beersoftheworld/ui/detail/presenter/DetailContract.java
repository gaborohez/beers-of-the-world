package com.gmail.gabow95k.beersoftheworld.ui.detail.presenter;

import com.gmail.gabow95k.beersoftheworld.base.BaseView;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;

import io.reactivex.Completable;

public interface DetailContract {
    interface View extends BaseView {

        void updated(Boolean flag);
    }

    interface Presenter {

        void update(BeerEntity beer);
    }

    interface Interactor {

        Completable addToFav(BeerEntity beer);
    }
}
