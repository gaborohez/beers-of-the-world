package com.gmail.gabow95k.beersoftheworld.ui.detail.presenter;

import com.gmail.gabow95k.beersoftheworld.base.BasePresenter;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;

public class DetailPresenter extends BasePresenter<DetailContract.View> implements DetailContract.Presenter {

    private DetailContract.Interactor interactor;

    public DetailPresenter(DetailContract.View view, DetailContract.Interactor interactor) {
        super(view);
        this.interactor = interactor;
    }

    @Override
    public void update(BeerEntity beer) {
        addSubscription(interactor.addToFav(beer)
                .doOnSubscribe(disposable -> view.showLoader())
                .doAfterTerminate(() -> view.hideLoader())
                .subscribe(() -> view.updated(beer.isFav()), Throwable::printStackTrace));
    }
}
