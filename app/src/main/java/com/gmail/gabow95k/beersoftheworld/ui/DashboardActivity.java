package com.gmail.gabow95k.beersoftheworld.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.gmail.gabow95k.beersoftheworld.R;
import com.gmail.gabow95k.beersoftheworld.base.BaseActivity;
import com.gmail.gabow95k.beersoftheworld.base.BasePresenter;
import com.gmail.gabow95k.beersoftheworld.constants.Constants;
import com.gmail.gabow95k.beersoftheworld.data.preferences.PreferencesManager;
import com.gmail.gabow95k.beersoftheworld.databinding.ActivityDashboardBinding;
import com.gmail.gabow95k.beersoftheworld.ui.beers.view.BeersFragment;
import com.gmail.gabow95k.beersoftheworld.ui.detail.view.DetailFragment;
import com.gmail.gabow95k.beersoftheworld.ui.favorites.view.FavoritesFragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class DashboardActivity extends BaseActivity<BasePresenter> {

    private ActivityDashboardBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDashboardBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.tvWelcome.setText(String.format(getString(R.string.welcome), PreferencesManager.getInstance().getString(Constants.USER_KEY_PREFS)));
        setFragment(new BeersFragment(), R.id.contentDashboard);

        setUpEvents();
    }

    private void setUpEvents() {
        binding.btnFab.setOnClickListener(v -> {
            addFragment(new FavoritesFragment(), R.id.contentDashboard);
        });

        binding.btnLogOut.setOnClickListener(v -> {
            showAlertDialogToLogOut();
        });

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                String tagName = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(tagName);
                checkFragment(fragment);
            } else {
                binding.btnFab.setVisibility(View.VISIBLE);
            }
        });
    }

    private void checkFragment(Fragment fragment) {
        if (fragment instanceof DetailFragment) {
            binding.btnFab.setVisibility(View.INVISIBLE);
        } else if (fragment instanceof FavoritesFragment) {
            binding.btnFab.setVisibility(View.INVISIBLE);
        }
    }

    private void showAlertDialogToLogOut() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.alert_logout));
        builder.setPositiveButton(getString(R.string.accept), (dialogInterface, i) -> {
            dialogInterface.dismiss();
            removePrefsAndGoToSession();
        });
        builder.setNegativeButton(getString(R.string.cancel), (dialogInterface, i) -> dialogInterface.dismiss());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void removePrefsAndGoToSession() {
        PreferencesManager.getInstance().removePreferences();

        Intent intent = new Intent(this, SessionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}