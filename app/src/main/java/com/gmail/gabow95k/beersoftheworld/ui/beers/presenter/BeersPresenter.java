package com.gmail.gabow95k.beersoftheworld.ui.beers.presenter;

import android.annotation.SuppressLint;

import com.gmail.gabow95k.beersoftheworld.base.BasePresenter;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;
import com.gmail.gabow95k.beersoftheworld.model.Beer;

import java.util.List;

import io.reactivex.Observable;

public class BeersPresenter extends BasePresenter<BeersContract.View> implements BeersContract.Presenter {

    private BeersContract.Interactor interactor;

    public BeersPresenter(BeersContract.View view, BeersContract.Interactor interactor) {
        super(view);
        this.interactor = interactor;
    }

    @Override
    public void checkIfExistDataInDB(int page) {
        addSubscription(interactor.checkIfExistDataInDB()
                .subscribe(flag -> {
                    if (flag) {
                        getBeersFromApi(page);
                    } else {
                        getBeersFromDB();
                    }
                }, throwable -> {
                    view.showErrorDialog(processError(throwable));
                }));
    }

    private void getBeersFromDB() {
        addSubscription(interactor.getBeersFromDB()
                .doOnSubscribe(disposable -> view.showLoader())
                .doAfterTerminate(() -> view.hideLoader())
                .subscribe(response -> {
                    if (response.isEmpty()) {
                        view.noBeers();
                    } else {
                        view.setBeers(response);
                    }
                }, throwable -> {
                    view.showErrorDialog(processError(throwable));
                }));
    }

    @Override
    public void getBeersFromApi(int page) {
        addSubscription(interactor.getBeers(page)
                .doOnSubscribe(disposable -> view.showLoader())
                .doAfterTerminate(() -> view.hideLoader())
                .subscribe(response -> {
                    if (response.isEmpty()) {
                        view.noBeers();
                    } else {
                        convertToEntity(response);
                    }
                }, throwable -> {
                    view.showErrorDialog(processError(throwable));
                }));
    }

    @SuppressLint("CheckResult")
    private void convertToEntity(List<Beer> beerList) {

        Observable.fromIterable(beerList)
                .map(beer -> {
                    BeerEntity entity = new BeerEntity();
                    entity.setName(beer.getName());
                    entity.setTagLine(beer.getTagline());
                    entity.setDescription(beer.getDescription());
                    entity.setFoodParing(beer.getFood_pairing());
                    entity.setTips(beer.getBrewers_tips());
                    entity.setImage(beer.getImage_url());
                    entity.setFav(false);
                    return entity;
                })
                .toList()
                .subscribe(beers -> {
                    insertInDB(beers);
                });
    }

    private void insertInDB(List<BeerEntity> beers) {
        addSubscription(interactor.insertInDB(beers)
                .subscribe(this::getBeersFromDB, Throwable::printStackTrace));
    }
}
