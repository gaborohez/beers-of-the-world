package com.gmail.gabow95k.beersoftheworld.ui.favorites.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gmail.gabow95k.beersoftheworld.R;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;
import com.gmail.gabow95k.beersoftheworld.databinding.ItemBeerBinding;

import java.util.ArrayList;
import java.util.List;

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.ViewHolder> implements Filterable {

    private Context context;
    private BeerIn listener;
    private List<BeerEntity> list, filterList;


    public FavoritesAdapter(Context context, List<BeerEntity> list, BeerIn listener) {
        this.context = context;
        this.listener = listener;
        this.list = list;
        this.filterList = list;
    }

    public interface BeerIn {
        void onItemClick(BeerEntity beer);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemBeerBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.footer.setBackgroundColor(context.getColor(R.color.orange));
        Glide.with(context)
                .load(list.get(position).getImage())
                .placeholder(context.getDrawable(R.drawable.placeholder))
                .into(holder.binding.ivBeer);
        holder.binding.tvName.setText(list.get(position).getName());
        holder.binding.content.setOnClickListener(v -> listener.onItemClick(list.get(position)));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemBeerBinding binding;

        public ViewHolder(ItemBeerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                list = filterList;

                if (!charString.isEmpty()) {
                    List<BeerEntity> auxList = new ArrayList<>();
                    for (BeerEntity item : list) {
                        if (item.getName().toLowerCase().contains(charString.toLowerCase())) {
                            auxList.add(item);
                        }
                    }
                    list = auxList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = list;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (List<BeerEntity>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
