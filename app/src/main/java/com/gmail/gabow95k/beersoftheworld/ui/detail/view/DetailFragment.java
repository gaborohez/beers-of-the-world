package com.gmail.gabow95k.beersoftheworld.ui.detail.view;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.gmail.gabow95k.beersoftheworld.R;
import com.gmail.gabow95k.beersoftheworld.base.BaseFragment;
import com.gmail.gabow95k.beersoftheworld.data.room.AppDB;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;
import com.gmail.gabow95k.beersoftheworld.databinding.FragmentDetailBinding;
import com.gmail.gabow95k.beersoftheworld.ui.detail.interactor.DetailInteractor;
import com.gmail.gabow95k.beersoftheworld.ui.detail.presenter.DetailContract;
import com.gmail.gabow95k.beersoftheworld.ui.detail.presenter.DetailPresenter;

public class DetailFragment extends BaseFragment<DetailContract.Presenter, FragmentDetailBinding> implements DetailContract.View {

    private BeerEntity beer;

    public DetailFragment(BeerEntity beer) {
        this.beer = beer;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DetailInteractor interactor = new DetailInteractor(AppDB.getAppDB(getActivity()).beerDAO());
        presenter = new DetailPresenter(this, interactor);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fillData();
        setUpEvents();
    }

    private void fillData() {
        Glide.with(requireActivity())
                .load(beer.isFav() ? R.drawable.ic_favorite : R.drawable.ic_favorite_border)
                .into(binding.btnAddToFab);

        Glide.with(requireActivity())
                .load(beer.getImage())
                .placeholder(requireContext().getDrawable(R.drawable.placeholder))
                .into(binding.ivBeer);
        binding.tvName.setText(beer.getName());
        binding.tvTagline.setText(beer.getTagLine());
        binding.tvDescription.setText(beer.getDescription());

        SpannableStringBuilder spannable = new SpannableStringBuilder();
        for (String item : beer.getFoodParing()) {
            spannable.append("*" + item + "\n");
        }
        binding.tvFoodPreparing.setText(spannable);
        binding.tvTips.setText(beer.getTips());
    }

    private void setUpEvents() {
        binding.btnAddToFab.setOnClickListener(v -> {
            beer.setFav(!beer.isFav());

            presenter.update(beer);
        });
    }

    @Override
    public void updated(Boolean flag) {
        Glide.with(requireActivity())
                .load(flag ? R.drawable.ic_favorite : R.drawable.ic_favorite_border)
                .into(binding.btnAddToFab);
    }
}