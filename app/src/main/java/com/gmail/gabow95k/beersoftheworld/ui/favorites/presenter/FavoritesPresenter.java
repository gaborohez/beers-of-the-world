package com.gmail.gabow95k.beersoftheworld.ui.favorites.presenter;

import com.gmail.gabow95k.beersoftheworld.base.BasePresenter;

public class FavoritesPresenter extends BasePresenter<FavoritesContract.View> implements FavoritesContract.Presenter {

    private FavoritesContract.Interactor interactor;

    public FavoritesPresenter(FavoritesContract.View view, FavoritesContract.Interactor interactor) {
        super(view);
        this.interactor = interactor;
    }

    @Override
    public void getFavorites() {
        addSubscription(interactor.getFavorites()
                .doOnSubscribe(disposable -> view.showLoader())
                .doAfterTerminate(() -> view.hideLoader())
                .subscribe(response -> {
                    if (response.isEmpty()) {
                        view.noBeers();
                    } else {
                        view.setBeers(response);
                    }
                }, throwable -> {
                    view.showErrorDialog(processError(throwable));
                }));
    }

}
