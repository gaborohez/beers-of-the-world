package com.gmail.gabow95k.beersoftheworld.ui.favorites.presenter;

import com.gmail.gabow95k.beersoftheworld.base.BaseView;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;

import java.util.List;

import io.reactivex.Single;

public interface FavoritesContract {
    interface View extends BaseView {

        void noBeers();

        void setBeers(List<BeerEntity> beers);
    }

    interface Presenter {

        void getFavorites();
    }

    interface Interactor {

        Single<List<BeerEntity>> getFavorites();
    }
}
