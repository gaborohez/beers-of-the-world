package com.gmail.gabow95k.beersoftheworld.data.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.gmail.gabow95k.beersoftheworld.constants.Constants;

@TypeConverters(StringListConverter.class)
@Database(entities = {BeerEntity.class}, version = 1)
public abstract class AppDB extends RoomDatabase {

    public static AppDB INSTANCE;

    public abstract BeerDAO beerDAO();

    public static AppDB getAppDB(final Context context) {

        if (INSTANCE == null) {

            synchronized (AppDB.class) {

                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDB.class, Constants.NAME_DATABASE)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
