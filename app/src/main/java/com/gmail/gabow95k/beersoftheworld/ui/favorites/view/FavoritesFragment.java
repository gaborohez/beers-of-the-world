package com.gmail.gabow95k.beersoftheworld.ui.favorites.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.gmail.gabow95k.beersoftheworld.R;
import com.gmail.gabow95k.beersoftheworld.base.BaseFragment;
import com.gmail.gabow95k.beersoftheworld.data.room.AppDB;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;
import com.gmail.gabow95k.beersoftheworld.databinding.FragmentFavoritesBinding;
import com.gmail.gabow95k.beersoftheworld.ui.beers.adapter.GridSpacingItemDecoration;
import com.gmail.gabow95k.beersoftheworld.ui.detail.view.DetailFragment;
import com.gmail.gabow95k.beersoftheworld.ui.favorites.adapter.FavoritesAdapter;
import com.gmail.gabow95k.beersoftheworld.ui.favorites.interactor.FavoritesInteractor;
import com.gmail.gabow95k.beersoftheworld.ui.favorites.presenter.FavoritesContract;
import com.gmail.gabow95k.beersoftheworld.ui.favorites.presenter.FavoritesPresenter;

import java.util.ArrayList;
import java.util.List;

public class FavoritesFragment extends BaseFragment<FavoritesContract.Presenter, FragmentFavoritesBinding> implements FavoritesContract.View, FavoritesAdapter.BeerIn {

    private int page = 1;
    private List<BeerEntity> list;
    private FavoritesAdapter adapter;
    private Boolean enableToRequestAPI = true;
    private Boolean readyToLoad = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = new ArrayList<>();
        FavoritesInteractor interactor = new FavoritesInteractor(AppDB.getAppDB(getActivity()).beerDAO());
        presenter = new FavoritesPresenter(this, interactor);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentFavoritesBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpRecycler();
        presenter.getFavorites();
    }


    private void setUpRecycler() {
        adapter = new FavoritesAdapter(getActivity(), list, this);
        binding.recycler.setLayoutManager(new GridLayoutManager(requireActivity(), 3));
        int spanCount = 3;
        int spacing = 50;
        boolean includeEdge = false;
        binding.recycler.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        binding.recycler.setHasFixedSize(true);
        binding.recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void noBeers() {
        binding.tvNoAvailable.setVisibility(View.VISIBLE);
        binding.recycler.setVisibility(View.GONE);
    }

    @Override
    public void setBeers(List<BeerEntity> beers) {
        readyToLoad = true;

        binding.tvNoAvailable.setVisibility(View.GONE);
        binding.recycler.setVisibility(View.VISIBLE);
        list.clear();
        list.addAll(beers);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onItemClick(BeerEntity beer) {
        addFragment(new DetailFragment(beer), R.id.contentDashboard);
    }
}