package com.gmail.gabow95k.beersoftheworld.data.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface BeerDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(BeerEntity beer);

    @Insert
    Completable insertAll(List<BeerEntity> beerList);

    @Query("SELECT * FROM beer_table")
    Single<List<BeerEntity>> getAll();

    @Delete
    Completable deleteItem(BeerEntity beer);

    @Update
    Completable update(BeerEntity beer);

    @Query("SELECT * FROM beer_table WHERE favorite =:flag")
    Single<List<BeerEntity>> getFavorites(Boolean flag);

    @Query("SELECT (SELECT COUNT(*) FROM beer_table) == 0")
    Single<Boolean> isEmpty();
}
