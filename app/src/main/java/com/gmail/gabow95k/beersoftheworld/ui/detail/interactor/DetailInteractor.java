package com.gmail.gabow95k.beersoftheworld.ui.detail.interactor;

import com.gmail.gabow95k.beersoftheworld.data.room.BeerDAO;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;
import com.gmail.gabow95k.beersoftheworld.ui.detail.presenter.DetailContract;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DetailInteractor implements DetailContract.Interactor {

    private BeerDAO beerDAO;

    public DetailInteractor(BeerDAO beerDAO) {
        this.beerDAO = beerDAO;
    }

    @Override
    public Completable addToFav(BeerEntity beer) {
        return beerDAO.update(beer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
