package com.gmail.gabow95k.beersoftheworld.ui.favorites.interactor;

import com.gmail.gabow95k.beersoftheworld.data.room.BeerDAO;
import com.gmail.gabow95k.beersoftheworld.data.room.BeerEntity;
import com.gmail.gabow95k.beersoftheworld.ui.favorites.presenter.FavoritesContract;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FavoritesInteractor implements FavoritesContract.Interactor {

    private BeerDAO beerDAO;

    public FavoritesInteractor(BeerDAO beerDAO) {
        this.beerDAO = beerDAO;
    }

    @Override
    public Single<List<BeerEntity>> getFavorites() {
        return beerDAO.getFavorites(true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
