package com.gmail.gabow95k.beersoftheworld.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.gmail.gabow95k.beersoftheworld.R;

public class BaseActivity<P> extends AppCompatActivity {

    protected P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void addFragment(Fragment fragment, int id) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right);
        transaction.add(id, fragment, fragment.getClass().getName());
        transaction.addToBackStack(fragment.getClass().getName());

        transaction.commitAllowingStateLoss();

    }

    protected void setFragment(Fragment fragment, int id) {
        getSupportFragmentManager().beginTransaction()
                .replace(id, fragment, fragment.getClass().getName())
                .commit();
    }

    protected void popBackStack() {
        getSupportFragmentManager().popBackStack();
    }
}
